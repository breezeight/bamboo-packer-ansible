
# Building Elastic Bamboo agents with Packer and Ansible #

This repository accompanies this blog post:

https://developer.atlassian.com/blog/2015/07/bamboo-packer/

The post and this code describes a simple method of building custom
Elastic Bamboo agents from using Packer and Ansible.
